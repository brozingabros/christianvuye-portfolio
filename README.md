[![Version](https://img.shields.io/badge/-v.1.0.1-brightgreen)](https://img.shields.io/badge/-v.1.0.1-brightgreen)
[![Netlify Status](https://api.netlify.com/api/v1/badges/a7cb9fbe-81f0-4418-b90c-09bd28863939/deploy-status)](https://app.netlify.com/sites/christianvuye/deploys)

# Christian Vuye Portfolio

A personal portfolio [site](https://www.christianvuye.net/) for Christian Vuye.

## Installation

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), and [Sanity](https://www.sanity.io/) CMS.

Use the package manager `yarn` to install and run.
