const FETCH_STATUS = {
  idle: 'idle',
  fetching: 'fetching',
  fetched: 'fetched'
};

export default FETCH_STATUS;
