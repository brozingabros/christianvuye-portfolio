import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { screen, render } from '@testing-library/react';
import Theme from './Theme';
import { Header } from './components';
import { routes } from './config/routes';

describe('renders components', () => {
  test('renders header', () => {
    render(
      <Router>
        <Theme>
          <Header routes={routes} />
        </Theme>
      </Router>
    );
    expect(screen.getByRole('banner').toBeVisible);
    expect(screen.getByRole('heading', { name: /christian vuye/i }).toBeVisible);
    expect(screen.getByText(/game producer/i).toBeVisible);
    expect(screen.getByText(/projects/i).toBeVisible);
    expect(screen.getByText(/resume/i).toBeVisible);
    expect(screen.getByText(/about/i).toBeVisible);
  });
});
