import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';
import Theme from './Theme';
import { routes, Routes } from './config/routes';
import { Header, Footer } from './components';
import { Container, Main } from './styles';
import GlobalStyle from './theme/globalStyles';

function App() {
  return (
    <HelmetProvider>
      <Router>
        <Theme>
          <GlobalStyle />
          <Container id='content'>
            <Header routes={routes} />
            <Main>
              <Routes />
            </Main>
          </Container>
          <Footer />
        </Theme>
      </Router>
    </HelmetProvider>
  );
}

export default App;
