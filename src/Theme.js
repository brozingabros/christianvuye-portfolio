import React from 'react';
import { ThemeProvider } from 'styled-components';

const theme = {
  colors: {
    black: '#000000',
    bostonBlue: '#3d85c5',
    secondary: '#6c757d',
    tertiary: 'rgba(0,0,0,0.1)',
    quaternary: '#fcfcfc'
  },
  fontFamily: {
    primary: 'Raleway, Arial, Helvetica, sans-serif',
    secondary: 'Hind Madurai, Arial, Helvetica, sans-serif',
    body: 'Oxygen, Arial, Helvetica, sans-serif'
  },
  fontSize: {
    body: '1rem',
    xxs: '1.4rem',
    xs: '1.75rem',
    sm: '2.1rem',
    md: '2.7rem',
    lg: '3.4rem',
    xl: '4.3rem'
  },
  kerning: {
    body: '1px',
    sm: '-0.5px',
    md: '-1px',
    lg: '-1.5px'
  },
  lineHeight: {
    body: '178%',
    sm: '109%',
    md: '110%',
    lg: '114%',
    xl: '124%'
  },
  screens: {
    sm: '640px',
    md: '768px',
    lg: '1024px',
    xl: '1280px'
  }
};

const Theme = ({ children }) => <ThemeProvider theme={theme}>{children}</ThemeProvider>;

export default Theme;
