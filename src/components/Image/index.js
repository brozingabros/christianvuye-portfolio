import styled from 'styled-components';

const Image = styled.img`
  margin-bottom: ${({ marginBottom }) => marginBottom};
  width: 100%;
`;

export default Image;
