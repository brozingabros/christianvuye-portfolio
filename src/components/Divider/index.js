import styled from 'styled-components';

const Divider = styled.hr`
  background-color: ${({ theme: { colors } }) => colors.tertiary};
  border: 0;
  height: 2px;
  width: 100%;
`;

export default Divider;
