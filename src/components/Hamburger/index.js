import React from 'react';
import PropTypes from 'prop-types';
import { MenuBtn, MenuBtnBurger } from './styles';

const Hamburger = ({ onClick, hamburgerState }) => {
  return (
    <MenuBtn onClick={onClick}>
      <MenuBtnBurger hamburgerState={hamburgerState} />
    </MenuBtn>
  );
};

Hamburger.propTypes = {
  hamburgerState: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Hamburger;
