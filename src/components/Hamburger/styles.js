import styled from 'styled-components';

export const MenuBtn = styled.div`
  align-items: center;
  display: flex;
  height: 50px;
  justify-content: center;
  position: relative;
  transition: all 0.5s ease-in-out;
  width: 50px;
  z-index: 2;
`;

export const MenuBtnBurger = styled.div`
  background: ${({ hamburgerState }) => (hamburgerState ? 'transparent' : ({ theme: { colors } }) => colors.black)};
  border-radius: 20px;
  height: 5px;
  transform: ${({ hamburgerState }) => hamburgerState && 'translateX(-50px)'};
  transition: all 0.5s ease-in-out;
  width: 50px;
  &:before {
    background: ${({ hamburgerState }) => (hamburgerState ? 'white' : ({ theme: { colors } }) => colors.black)};
    border-radius: 20px;
    content: '';
    height: 5px;
    position: absolute;
    transform: ${({ hamburgerState }) =>
      hamburgerState ? `rotate(45deg) translate(35px,-35px)` : `translateY(-16px)`};
    transition: all 0.5s ease-in-out;
    width: 50px;
  }
  &:after {
    background: ${({ hamburgerState }) => (hamburgerState ? 'white' : ({ theme: { colors } }) => colors.black)};
    border-radius: 20px;
    content: '';
    height: 5px;
    position: absolute;
    transform: ${({ hamburgerState }) => (hamburgerState ? `rotate(-45deg) translate(35px,35px)` : `translateY(16px)`)};
    transition: all 0.5s ease-in-out;
    width: 50px;
  }
`;
