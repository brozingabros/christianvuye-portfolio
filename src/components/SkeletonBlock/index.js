import styled from 'styled-components';

const SkeletonBlock = styled.div`
  margin-bottom: ${({ marginBottom }) => marginBottom};
  width: ${({ widthMobile }) => widthMobile};
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    width: ${({ width }) => width};
  }
`;

export default SkeletonBlock;
