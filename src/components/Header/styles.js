import styled from 'styled-components';

const HeaderContainer = styled.header`
  padding: 2rem 0 0 0;
`;

export default HeaderContainer;
