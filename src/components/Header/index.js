import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import HeaderContainer from './styles';
import Row from '../Row';
import RowItem from '../RowItem';
import Logo from '../Logo';
import Navigation from '../Navigation';

const Header = ({ routes }) => {
  const [clientWidth, setClientWidth] = useState(window.innerWidth);

  useEffect(() => {
    const updateClientWidth = () => setClientWidth(window.innerWidth);
    window.addEventListener('resize', updateClientWidth);
    return () => window.removeEventListener('resize', updateClientWidth);
  }, []);

  return (
    <HeaderContainer>
      <Row row>
        <RowItem width='50%' widthMobile='80%'>
          <Logo />
        </RowItem>
        <RowItem
          alignItems={clientWidth > 768 ? 'flex-end' : 'flex-start'}
          justifyContent={clientWidth > 768 ? 'flex-end' : 'flex-end'}
          width='50%'
          widthMobile='20%'>
          <Navigation clientWidth={clientWidth} routes={routes} />
        </RowItem>
      </Row>
    </HeaderContainer>
  );
};

Header.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Header;
