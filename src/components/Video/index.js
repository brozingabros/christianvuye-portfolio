import React from 'react';
import PropTypes from 'prop-types';
import { Player, VideoContainer } from './styles';

const Video = ({ url }) => {
  return (
    <VideoContainer>
      <Player controls url={url} width='100%' height='100%' />
    </VideoContainer>
  );
};

Video.propTypes = {
  url: PropTypes.string.isRequired
};

export default Video;
