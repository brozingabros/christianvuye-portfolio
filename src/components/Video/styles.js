import styled from 'styled-components';
import ReactPlayer from 'react-player';

export const VideoContainer = styled.div`
  padding-top: 56.25%;
  position: relative;
`;

export const Player = styled(ReactPlayer)`
  left: 0;
  position: absolute;
  top: 0;
`;
