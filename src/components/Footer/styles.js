import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FaEnvelope, FaLinkedin, FaPhoneSquareAlt } from 'react-icons/fa';
import Obfuscate from 'react-obfuscate';
import RowItem from '../RowItem';
import { ExternalLink } from '../Link';

export const FooterContainer = styled.footer`
  background-color: ${({ theme: { colors } }) => colors.quaternary};
  margin: 8rem 0 0 0;
  padding: 2rem 0;
`;

const ContactBlockContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-family: ${({ theme: { fontFamily } }) => fontFamily.body};
  font-weight: 700;
  justify-content: center;
  margin: 0 0 0 1rem;
`;

const ContactBlockItem = styled.span`
  display: block;
  margin: 0;
`;

const StyledObfuscate = styled(Obfuscate)`
  color: ${({ theme: { colors } }) => colors.black};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.body};
  text-decoration: none;
`;

export const ContactBlock = ({ children, type, url }) => {
  return (
    <RowItem flexDirectionMobile='row' justifyContent='space-between'>
      {type === 'tel' && <FaPhoneSquareAlt size='2em' />}
      {type === 'email' && <FaEnvelope size='2em' />}
      {type === 'linkedin' && <FaLinkedin size='2em' />}
      <ContactBlockContainer>
        <ContactBlockItem>
          {type === 'tel' && <StyledObfuscate tel={children} />}
          {type === 'email' && <StyledObfuscate email={children} />}
          {type === 'linkedin' && (
            <ExternalLink href={url} target='_blank' rel='noreferrer noopener' style={{ color: 'black' }}>
              {children}
            </ExternalLink>
          )}
        </ContactBlockItem>
      </ContactBlockContainer>
    </RowItem>
  );
};

ContactBlock.defaultProps = {
  url: ''
};

ContactBlock.propTypes = {
  type: PropTypes.string.isRequired,
  url: PropTypes.string
};
