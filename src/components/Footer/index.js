import React from 'react';
import useFetch from '../../utils/hooks';
import FETCH_STATUS from '../../constants';
import { ContactBlock, FooterContainer } from './styles';
import { Container } from '../../styles';
import Row from '../Row';

const Footer = () => {
  const query = `*[_type == "contactSettings" ] | {main, social}`;
  const { response } = useFetch(query);
  const isFetched = response && response.status === FETCH_STATUS.fetched;
  const isDefined = value => value !== undefined;

  return (
    <FooterContainer>
      <Container>
        <Row flexDirectionMobile='column' flexDirection='row'>
          {isFetched && isDefined(response.data[0].main.email) && (
            <ContactBlock type='email'>{response.data[0].main.email}</ContactBlock>
          )}
          {isFetched && isDefined(response.data[0].main.phone) && (
            <ContactBlock type='tel'>{response.data[0].main.phone.toString()}</ContactBlock>
          )}
          {isFetched && isDefined(response.data[0].social.linkedIn.linkedInUsername) && (
            <ContactBlock url={response.data[0].social.linkedIn.linkedInLink} type='linkedin'>
              {response.data[0].social.linkedIn.linkedInUsername}
            </ContactBlock>
          )}
        </Row>
      </Container>
    </FooterContainer>
  );
};

export default Footer;
