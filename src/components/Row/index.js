import styled from 'styled-components';

const Row = styled.div`
  display: flex;
  flex-direction: ${({ flexDirectionMobile }) => flexDirectionMobile};
  flex-wrap: ${({ flexWrap }) => (flexWrap ? 'wrap' : 'nowrap')};
  justify-content: space-between;
  margin-top: ${({ mt }) => mt};
  width: 100%;
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    flex-direction: ${({ flexDirection }) => flexDirection};
    width: ${({ width }) => width};
  }
`;

export default Row;
