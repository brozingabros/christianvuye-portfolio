import styled from 'styled-components';

const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.bostonBlue};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.primary};
  font-size: ${({ theme: { fontSize } }) => fontSize.xxs};
  margin: 0;
`;

export default Title;
