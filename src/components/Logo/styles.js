import styled from 'styled-components';

export const LogoTitle = styled.h1`
  color: ${({ theme: { colors } }) => colors.black};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.primary};
  font-size: ${({ theme: { fontSize } }) => fontSize.md};
  font-weight: 800;
  letter-spacing: ${({ theme: { kerning } }) => kerning.md};
  line-height: ${({ theme: { lineHeight } }) => lineHeight.sm};
  margin: 0;
  span {
    color: ${({ theme: { colors } }) => colors.bostonBlue};
  }
`;

export const LogoSubTitle = styled.p`
  color: ${({ theme: { colors } }) => colors.black};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.secondary};
  font-size: ${({ theme: { fontSize } }) => fontSize.xxs};
  font-weight: 400;
  letter-spacing: ${({ theme: { kerning } }) => kerning.md};
  line-height: ${({ theme: { lineHeight } }) => lineHeight.sm};
  margin: 0.5rem 0 0 0;
`;
