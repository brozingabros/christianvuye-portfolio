import React from 'react';
import { RouterLink } from '../Link';
import { LogoTitle, LogoSubTitle } from './styles';

const Logo = () => {
  return (
    <section>
      <RouterLink to='/'>
        <LogoTitle>
          Christian <span>Vuye</span>
        </LogoTitle>
        <LogoSubTitle>Game Producer</LogoSubTitle>
      </RouterLink>
    </section>
  );
};

export default Logo;
