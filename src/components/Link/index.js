import styled from 'styled-components';
import { Link, NavLink } from 'react-router-dom';

export const RouterLink = styled(Link)`
  color: ${({ theme: { colors } }) => colors.secondary};
  text-decoration: none;
`;

export const RouterNavLink = styled(NavLink)`
  color: ${({ mobile }) => (mobile ? 'white' : ({ theme: { colors } }) => colors.black)};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.secondary};
  font-size: ${({ mobile }) =>
    mobile ? ({ theme: { fontSize } }) => fontSize.sm : ({ theme: { fontSize } }) => fontSize.body};
  margin: ${({ mobile }) => (mobile ? '1rem' : '0 1rem')};
  text-decoration: none;
  &.active {
    color: ${({ mobile }) =>
      mobile ? ({ theme: { colors } }) => colors.quaternary : ({ theme: { colors } }) => colors.bostonBlue};
  }
`;

export const ExternalLink = styled.a`
  color: ${({ theme: { colors } }) => colors.secondary};
  text-decoration: none;
`;
