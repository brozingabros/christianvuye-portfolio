import styled from 'styled-components';
import BlockContent from '@sanity/block-content-to-react';

const TextBlock = styled(BlockContent)`
  color: ${({ theme: { colors } }) => colors.secondary};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.secondary};
`;

export default TextBlock;
