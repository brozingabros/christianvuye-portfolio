import styled from 'styled-components';

const ContentBlock = styled.article`
  color: ${({ theme: { colors } }) => colors.secondary};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.secondary};
`;

export default ContentBlock;
