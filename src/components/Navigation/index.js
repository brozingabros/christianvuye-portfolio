import React from 'react';
import PropTypes from 'prop-types';
import { MobileNavigation, DesktopNavigation } from './styles';

const Navigation = ({ clientWidth, routes }) => {
  return clientWidth > 768 ? <DesktopNavigation routes={routes} /> : <MobileNavigation routes={routes} />;
};

Navigation.propTypes = {
  clientWidth: PropTypes.number.isRequired,
  routes: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Navigation;
