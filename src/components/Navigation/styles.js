import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Locky from 'react-locky';
import styled from 'styled-components';
import { RouterNavLink } from '../Link';
import Hamburger from '../Hamburger';

const NavigationList = styled.ul`
  align-items: ${({ mobile }) => mobile && 'center'};
  background-color: ${({ mobile, theme: { colors } }) => mobile && colors.bostonBlue};
  display: flex;
  flex-direction: ${({ mobile }) => mobile && 'column'};
  height: ${({ mobile }) => mobile && '100%'};
  justify-content: ${({ mobile }) => mobile && 'center'};
  left: ${({ mobile }) => mobile && '0'};
  list-style: none;
  margin: 0;
  padding: 0;
  position: ${({ mobile }) => mobile && 'fixed'};
  top: ${({ mobile }) => mobile && '0'};
  transform: ${({ mobile, hamburgerState }) => mobile && (hamburgerState ? 'translateX(0)' : 'translateX(-100%)')};
  transition: ${({ mobile }) => mobile && 'transform 0.3s ease-in-out'};
  width: ${({ mobile }) => mobile && '100%'};
  z-index: ${({ mobile }) => mobile && '1'};
`;

const NavigationItem = styled.li`
  color: ${({ mobile }) => (mobile ? 'white' : ({ theme: { colors } }) => colors.black)};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.secondary};
  font-size: ${({ mobile }) =>
    mobile ? ({ theme: { fontSize } }) => fontSize.sm : ({ theme: { fontSize } }) => fontSize.body};
  margin: ${({ mobile }) => (mobile ? '1rem' : '0 1rem')};

  &:first-child {
    margin: ${({ mobile }) => !mobile && '0 1rem 0 0'};
  }

  &:last-child {
    margin: ${({ mobile }) => !mobile && '0 0 0 1rem'};
  }
`;

const NavigationLinkMobile = ({ children, onClick, to }) => {
  return (
    <RouterNavLink exact mobile='true' to={to} onClick={() => onClick()}>
      {children}
    </RouterNavLink>
  );
};

export const MobileNavigation = ({ routes }) => {
  const [hamburgerState, setHamburgerState] = useState(false);
  const handleHamburgerState = () => (hamburgerState ? setHamburgerState(false) : setHamburgerState(true));

  return (
    <>
      <Locky enabled={hamburgerState}>
        <Hamburger onClick={handleHamburgerState} hamburgerState={hamburgerState} />
        <nav>
          <NavigationList mobile hamburgerState={hamburgerState}>
            {routes.map(route => (
              <NavigationItem mobile key={route.id}>
                <NavigationLinkMobile onClick={handleHamburgerState} to={route.path}>
                  {route.text}
                </NavigationLinkMobile>
              </NavigationItem>
            ))}
          </NavigationList>
        </nav>
      </Locky>
    </>
  );
};

export const DesktopNavigation = ({ routes }) => {
  return (
    <nav>
      <NavigationList>
        {routes.map(route => (
          <NavigationItem key={route.id}>
            <RouterNavLink exact to={route.path} activeClassName='active'>
              {route.text}
            </RouterNavLink>
          </NavigationItem>
        ))}
      </NavigationList>
    </nav>
  );
};

NavigationLinkMobile.propTypes = {
  onClick: PropTypes.func.isRequired,
  to: PropTypes.string.isRequired
};

MobileNavigation.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired
};

DesktopNavigation.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired
};
