import styled from 'styled-components';

const RowItem = styled.div`
  align-items: ${({ alignItems }) => alignItems};
  display: flex;
  flex-direction: ${({ flexDirectionMobile }) => flexDirectionMobile};
  justify-content: ${({ justifyContent }) => justifyContent};
  width: ${({ widthMobile }) => widthMobile};
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    flex-direction: ${({ flexDirection }) => flexDirection};
    width: ${({ width }) => width};
  }
`;

export default RowItem;
