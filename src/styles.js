import styled from 'styled-components';

export const Container = styled.div`
  margin: auto;
  width: 90%;
  @media (min-width: ${({ theme: { screens } }) => screens.lg}) {
    margin: auto;
    width: 66%;
  }
`;

export const Main = styled.main`
  margin: 4rem 0 0 0;
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    margin: 8rem 0 0 0;
  }
`;

export const Row = styled.div`
  display: flex;
  flex-direction: ${({ flexDirectionMobile }) => flexDirectionMobile};
  flex-wrap: ${({ flexWrap }) => (flexWrap ? 'wrap' : 'nowrap')};
  justify-content: space-between;
  margin-top: ${({ mt }) => mt};
  width: 100%;
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    flex-direction: ${({ flexDirection }) => flexDirection};
    width: ${({ width }) => width};
  }
`;

export const RowItem = styled.div`
  align-items: ${({ alignItems }) => alignItems};
  display: flex;
  flex-direction: ${({ flexDirectionMobile }) => flexDirectionMobile};
  justify-content: ${({ justifyContent }) => justifyContent};
  width: ${({ widthMobile }) => widthMobile};
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    flex-direction: ${({ flexDirection }) => flexDirection};
    width: ${({ width }) => width};
  }
`;

export const HorizontalLine = styled.hr`
  background-color: ${({ theme: { colors } }) => colors.tertiary};
  border: 0;
  height: 2px;
  width: 100%;
`;
