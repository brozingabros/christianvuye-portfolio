import sanityClient from '@sanity/client';

export default sanityClient({
  projectId: '2oq7z3yn',
  dataset: 'production',
  useCdn: true
});
