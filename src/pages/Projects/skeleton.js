import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Row, RowItem, SkeletonBlock } from '../../components';

const ProjectsSkeleton = () => {
  return (
    <Row column flexWrap width='100%' style={{ marginBottom: '6rem' }}>
      <RowItem
        flexDirectionMobile='column'
        flexDirection='row'
        justifyContent='space-between'
        width='100%'
        widthMobile='100%'>
        <SkeletonBlock width='48%' widthMobile='100%'>
          <Skeleton height={225} width='100%' />
        </SkeletonBlock>
        <SkeletonBlock width='48%' widthMobile='100%'>
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton height={135} />
        </SkeletonBlock>
      </RowItem>
    </Row>
  );
};

export default ProjectsSkeleton;
