import React from 'react';
import { Helmet } from 'react-helmet-async';
import FETCH_STATUS from '../../constants';
import useFetch from '../../utils/hooks';
import handleDateFormat from '../../utils/date';
import { ContentBlock, TextBlock, Row, RowItem, Divider } from '../../components';
import { handleProjectMedia, ProjectDetail, ProjectMedia, ProjectDetails, ProjectList, Project } from './styles';
import ProjectsSkeleton from './skeleton';

const Projects = () => {
  const query = `*[_type == "project"] | order(order asc) { _id, name, role, startDate, endDate, teamSize, platform, framework, video, mainImage{asset->{_id,url}, altText}, body}`;
  const { response } = useFetch(query);
  const isFetched = response && response.status === FETCH_STATUS.fetched;

  return (
    <>
      <Helmet>
        <title>Projects</title>
        <meta property='og:image' content='/og-image.jpg' />
        <meta property='og:image:width' content='273' />
        <meta property='og:image:height' content='409' />
        <meta property='og:title' content='Christian Vuye | Game Producer - Projects' />
        <meta property='og:description' content='The projects page of the game producer Christian Vuye.' />
        <meta property='og:url' content='https://www.christianvuye.net/projects' />
      </Helmet>
      <Row column flexWrap width='100%'>
        {isFetched ? (
          response.data.map(project => (
            <Project key={project._id}>
              <RowItem
                flexDirectionMobile='column'
                flexDirection='row'
                justifyContent='space-between'
                widthMobile='100%'>
                <ProjectMedia>{handleProjectMedia(project)}</ProjectMedia>
                <ProjectDetails>
                  <ProjectList>
                    {project.name && <ProjectDetail title='Game:'>{project.name}</ProjectDetail>}
                    {project.role && <ProjectDetail title='Role:'>{project.role}</ProjectDetail>}
                    {project.startDate && (
                      <ProjectDetail title='Period:'>
                        {handleDateFormat(project.startDate, project.endDate)}
                      </ProjectDetail>
                    )}
                    {project.teamSize && <ProjectDetail title='Team Size:'>{project.teamSize}</ProjectDetail>}
                    {project.platform && <ProjectDetail title='Platform:'>{project.platform}</ProjectDetail>}
                    {project.framework && <ProjectDetail title='Framework:'>{project.framework}</ProjectDetail>}
                  </ProjectList>
                  {project.body && (
                    <ContentBlock>
                      <TextBlock blocks={project.body} />
                    </ContentBlock>
                  )}
                </ProjectDetails>
              </RowItem>
              <Divider />
            </Project>
          ))
        ) : (
          <>
            <ProjectsSkeleton />
            <ProjectsSkeleton />
            <ProjectsSkeleton />
            <ProjectsSkeleton />
          </>
        )}
      </Row>
    </>
  );
};

export default Projects;
