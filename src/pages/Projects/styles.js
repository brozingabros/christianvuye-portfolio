import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Image, Video } from '../../components';

export const Project = styled.article`
  margin: 0 0 4rem 0;
  width: 100%;
`;

export const ProjectMedia = styled.div`
  margin: 0 0 1rem 0;
  width: 100%;
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    width: 48%;
  }
`;

const ProjectMediaPlaceholder = styled.div`
  width: 100%;
`;

export const ProjectDetails = styled.div`
  width: 100%;
  @media (min-width: ${({ theme: { screens } }) => screens.md}) {
    width: 48%;
  }
`;

export const ProjectList = styled.dl`
  color: ${({ theme: { colors } }) => colors.secondary};
  font-family: ${({ theme: { fontFamily } }) => fontFamily.secondary};
  list-style: none;
  margin: 0;
  padding: 0;
`;

const ProjectItem = styled.div`
  display: flex;
  justify-content: space-between;
  text-align: left;
`;

const ProjectItemTitle = styled.dt`
  font-weight: 700;
`;

const ProjectItemValue = styled.dd`
  text-align: right;
`;

export const ProjectDetail = ({ children, title }) => {
  return (
    <ProjectItem>
      <ProjectItemTitle>{title}</ProjectItemTitle>
      <ProjectItemValue>{children}</ProjectItemValue>
    </ProjectItem>
  );
};

export const handleProjectMedia = media => {
  if (media.video) {
    return <Video url={media.video} />;
  }

  if (media.mainImage && media.mainImage.asset) {
    return (
      <Image
        src={media.mainImage.asset.url && media.mainImage.asset.url}
        alt={media.mainImage.altText && media.mainImage.altText}
      />
    );
  }

  return <ProjectMediaPlaceholder />;
};

ProjectDetail.propTypes = {
  title: PropTypes.string.isRequired
};
