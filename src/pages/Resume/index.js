import React from 'react';
import { Helmet } from 'react-helmet-async';
import { GrDocumentPdf } from 'react-icons/gr';
import FETCH_STATUS from '../../constants';
import useFetch from '../../utils/hooks';
import { ContentBlock, ExternalLink, TextBlock, Title, Row, RowItem } from '../../components';
import { SkeletonDownloadResume, SkeletonLeftColumn, SkeletonRightColumn } from './skeleton';

const COLUMN = {
  left: 'left',
  main: 'main'
};

const Resume = () => {
  const query = `*[_type in ["resume", "resumeSettings"]] | order(order asc) { _id, title , body, column, downloadCheck, downloadText, "downloadFile": downloadFile.asset->url}`;
  const { response } = useFetch(query);
  const isFetched = response && response.status === FETCH_STATUS.fetched;

  return (
    <>
      <Helmet>
        <title>Resume</title>
        <meta property='og:image' content='/og-image.jpg' />
        <meta property='og:image:width' content='273' />
        <meta property='og:image:height' content='409' />
        <meta property='og:title' content='Christian Vuye | Game Producer - Resume' />
        <meta property='og:description' content='The resume page of the game producer Christian Vuye.' />
        <meta property='og:url' content='https://www.christianvuye.net/resume' />
      </Helmet>
      <Row flexDirectionMobile='column-reverse' flexDirection='row' flexWrap mt='5rem'>
        <RowItem flexDirectionMobile='column' width='40%' widthMobile='100%'>
          {isFetched ? (
            response.data.map(
              block =>
                block.column === COLUMN.left && (
                  <ContentBlock key={block._id}>
                    <Title>{block.title}</Title>
                    <TextBlock blocks={block.body} />
                  </ContentBlock>
                )
            )
          ) : (
            <SkeletonLeftColumn />
          )}
          {isFetched && response.data[response.data.length - 1].downloadCheck === true ? (
            <ContentBlock>
              <ExternalLink href={`${response.data[response.data.length - 1].downloadFile}?dl=`}>
                <GrDocumentPdf /> {response.data[response.data.length - 1].downloadText}
              </ExternalLink>
            </ContentBlock>
          ) : (
            <SkeletonDownloadResume />
          )}
        </RowItem>
        <RowItem flexDirectionMobile='column' width='55%' widthMobile='100%'>
          {isFetched ? (
            response.data.map(
              block =>
                block.column === COLUMN.main && (
                  <ContentBlock key={block._id}>
                    <Title>{block.title}</Title>
                    <TextBlock blocks={block.body} />
                  </ContentBlock>
                )
            )
          ) : (
            <SkeletonRightColumn />
          )}
        </RowItem>
      </Row>
    </>
  );
};

export default Resume;
