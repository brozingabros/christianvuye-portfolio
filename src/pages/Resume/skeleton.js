import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { SkeletonBlock } from '../../components';

export const SkeletonLeftColumn = () => {
  return (
    <>
      <SkeletonBlock marginBottom='1rem'>
        <Skeleton height={30} />
        <Skeleton count={2} height={20} />
      </SkeletonBlock>
      <SkeletonBlock marginBottom='1rem'>
        <Skeleton height={30} />
        <Skeleton count={3} height={20} />
      </SkeletonBlock>
      <SkeletonBlock marginBottom='1rem'>
        <Skeleton height={30} />
        <Skeleton count={5} height={20} />
      </SkeletonBlock>
      <SkeletonBlock marginBottom='1rem'>
        <Skeleton height={30} />
        <Skeleton count={14} height={20} />
      </SkeletonBlock>
    </>
  );
};

export const SkeletonDownloadResume = () => (
  <SkeletonBlock marginBottom='1rem'>
    <Skeleton height={30} />
  </SkeletonBlock>
);

export const SkeletonRightColumn = () => {
  return (
    <>
      <SkeletonBlock marginBottom='1rem'>
        <Skeleton height={30} />
        <Skeleton count={3} height={60} />
      </SkeletonBlock>
      <SkeletonBlock marginBottom='1rem'>
        <Skeleton height={30} />
        <Skeleton count={1} height={40} />
        <Skeleton count={1} height={60} />
        <Skeleton height={30} />
        <Skeleton count={1} height={40} />
        <Skeleton count={1} height={80} />
        <Skeleton height={30} />
        <Skeleton count={1} height={40} />
        <Skeleton count={1} height={80} />
      </SkeletonBlock>
    </>
  );
};
