import React from 'react';
import { Helmet } from 'react-helmet-async';
import Skeleton from 'react-loading-skeleton';
import useFetch from '../../utils/hooks';
import FETCH_STATUS from '../../constants';
import { TextBlock, Image, Row, RowItem, Title } from '../../components';

const About = () => {
  const query = `*[_type == "about"] | { _id, body, mainImage{asset->{_id,url}, altText} }`;
  const { response } = useFetch(query);
  const isFetched = response && response.status === FETCH_STATUS.fetched;

  return (
    <>
      <Helmet>
        <title>About</title>
        <meta property='og:image' content='/og-image.jpg' />
        <meta property='og:image:width' content='273' />
        <meta property='og:image:height' content='409' />
        <meta property='og:title' content='Christian Vuye | Game Producer - About' />
        <meta property='og:description' content='The about page of the game producer Christian Vuye.' />
        <meta property='og:url' content='https://www.christianvuye.net/about' />
      </Helmet>
      <Row flexDirectionMobile='column' flexDirection='row' flexWrap mt='5rem'>
        <RowItem flexDirectionMobile='column' width='30%' widthMobile='100%'>
          {isFetched ? (
            <Image
              src={response.data[0].mainImage.asset.url}
              alt={response.data[0].mainImage.altText}
              marginBottom='1rem'
            />
          ) : (
            <Skeleton height={500} width='100%' />
          )}
        </RowItem>
        <RowItem flexDirectionMobile='column' width='65%' widthMobile='100%'>
          {isFetched ? (
            <>
              <Title>About</Title>
              {response.data.map(block => (
                <TextBlock blocks={block.body} key={block._id} />
              ))}
            </>
          ) : (
            <>
              <Skeleton height={30} style={{ marginBottom: '10px' }} />
              <Skeleton height={74} count={5} style={{ marginBottom: '10px' }} />
            </>
          )}
        </RowItem>
      </Row>
    </>
  );
};

export default About;
