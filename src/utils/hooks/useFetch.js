import { useCallback, useEffect, useReducer } from 'react';
import sanityClient from '../../client';
import FETCH_STATUS from '../../constants';

const reducerInitializer = initialState => {
  return {
    status: initialState,
    data: {}
  };
};

const fetchStatusReducer = (state, action) => {
  switch (action.type) {
    case FETCH_STATUS.idle:
      return { ...state, status: FETCH_STATUS.idle };
    case FETCH_STATUS.fetching:
      return { ...state, status: FETCH_STATUS.fetching };
    case FETCH_STATUS.fetched:
      return { ...state, status: FETCH_STATUS.fetched, data: action.payload.response };
    default:
      return state;
  }
};

const useFetch = query => {
  const [currentState, dispatch] = useReducer(fetchStatusReducer, FETCH_STATUS.idle, reducerInitializer);

  const fetchData = useCallback(() => {
    dispatch({ type: FETCH_STATUS.fetching });
    sanityClient
      .fetch(query)
      .then(response => {
        dispatch({ type: FETCH_STATUS.fetched, payload: { response } });
      })
      .catch(error => Error(error));
  }, [query]);

  useEffect(() => {
    if (!query) return;
    fetchData();
  }, [query, fetchData]);

  return { response: currentState };
};

export default useFetch;
