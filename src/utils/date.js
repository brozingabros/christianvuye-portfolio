const handleReverseDateFormat = date => date && date.split('-').reverse().join('-');
const handlePeriodToMonthConvert = (startDate, endDate) => {
  const date01 = new Date(startDate);
  const date02 = new Date(endDate);

  let months;
  months = (date02.getFullYear() - date01.getFullYear()) * 12;
  months -= date01.getMonth();
  months += date02.getMonth();
  return months <= 0 ? `1 month` : `${months} months`;
};

const handleDateFormat = (startDate, endDate) => {
  if (!endDate) {
    return `${handleReverseDateFormat(startDate)} - Present`;
  }

  return handlePeriodToMonthConvert(startDate, endDate);
};

export default handleDateFormat;
