import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html {
    overflow-y: scroll;
  }

  html, body, #root {
  height: 100%;
}

  #root {
  display: flex;
  flex-direction: column;
  }
  
  #content {
    flex: 1 0 auto;
  }
  
  footer {
  flex-shrink: 0;
  }
`;

export default GlobalStyle;
