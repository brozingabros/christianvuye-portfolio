import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Projects from '../pages/Projects';
import Resume from '../pages/Resume';
import About from '../pages/About';

export const routes = [
  { component: Projects, id: 1, path: '/', text: 'Projects' },
  { component: Resume, id: 2, path: '/resume', text: 'Resume' },
  { component: About, id: 3, path: '/about', text: 'About' }
];

export const Routes = () => {
  return (
    <Switch>
      {routes.map(route => (
        <Route exact component={route.component} key={route.id} path={route.path} />
      ))}
    </Switch>
  );
};
