import { FcViewDetails } from 'react-icons/fc';

export default {
  name: 'project',
  title: 'Projects',
  type: 'document',
  icon: FcViewDetails,
  fields: [
    {
      name: 'name',
      description: 'Add a project name',
      title: 'Project Name',
      type: 'string',
      validation: Rule => Rule.required().error('You need to add a project name')
    },
    { name: 'role', title: 'Role', type: 'string' },
    {
      name: 'startDate',
      title: 'Start Date',
      type: 'date',
      options: { dateFormat: 'DD-MM-YYYY' }
    },
    {
      name: 'endDate',
      title: 'End Date',
      type: 'date',
      options: { dateFormat: 'DD-MM-YYYY' }
    },
    { name: 'teamSize', title: 'Team size', type: 'string' },
    { name: 'platform', title: 'Platform', type: 'string' },
    { name: 'framework', title: 'Framework', type: 'string' },
    {
      name: 'video',
      title: 'Video URL',
      type: 'url',
      description:
        'Supported video services: Youtube, Vimeo, Soundcloud, Facebook, Wistia, Mixcloud, Dailymotion, Twitch',
      validation: Rule =>
        Rule.uri({
          scheme: ['http', 'https']
        }).error('Your URL needs to start with "http:// or https://"')
    },
    {
      name: 'mainImage',
      title: 'Main image',
      type: 'image',
      options: {
        hotspot: true
      },
      fields: [{ name: 'altText', title: 'Image Alt Text', type: 'string' }]
    },
    {
      name: 'body',
      title: 'Body',
      type: 'blockContent'
    },
    {
      name: 'order',
      title: 'Order',
      type: 'number',
      hidden: true
    }
  ],
  orderings: [
    {
      title: 'Start Date Desc',
      name: 'startDateDesc',
      by: [{ field: 'startDate', direction: 'desc' }]
    },
    {
      title: 'Start Date Asc',
      name: 'startDateAsc',
      by: [{ field: 'startDate', direction: 'asc' }]
    }
  ],

  preview: {
    select: {
      title: 'name'
    },
    prepare(selection) {
      const { author } = selection;
      return { ...selection, subtitle: author && `by ${author}` };
    }
  }
};
