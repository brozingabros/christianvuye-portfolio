import { FcFolder } from 'react-icons/fc';

export default {
  name: 'about',
  title: 'About',
  type: 'document',
  __experimental_actions: ['update', 'publish'],
  icon: FcFolder,
  fields: [
    {
      name: 'body',
      title: 'Biography',
      description: 'Add profile biography',
      type: 'blockContent'
    },
    {
      name: 'mainImage',
      title: 'Profile Image',
      type: 'image',
      options: {
        hotspot: true
      },
      fields: [{ name: 'altText', title: 'Image Alt Text', type: 'string' }]
    }
  ]
};
