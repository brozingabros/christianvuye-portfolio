import { FcFolder } from 'react-icons/fc';

export default {
  name: 'contactSettings',
  title: 'Contact Settings',
  type: 'document',
  __experimental_actions: ['update', 'publish'],
  icon: FcFolder,
  fields: [
    {
      name: 'main',
      title: 'Main',
      type: 'object',

      fields: [
        {
          name: 'email',
          description: 'Add your email address.',
          title: 'Email',
          type: 'string',
          placeholder: 'username@domain.com'
        },
        {
          name: 'phone',
          description: 'Add your phone number.',
          title: 'Phone Number:',
          type: 'number',
          placeholder: '+32486731030'
        }
      ]
    },
    {
      name: 'social',
      title: 'Social',
      type: 'object',
      fieldsets: [
        {
          name: 'social',
          title: 'Social Media'
        }
      ],

      fields: [
        {
          name: 'linkedIn',
          title: 'LinkedIn',
          type: 'object',
          fieldset: 'social',
          options: {
            collapsible: true,
            collapsed: false
          },
          fields: [
            {
              name: 'linkedInUsername',
              title: 'Username:',
              type: 'string',
              placeholder: '@username',
              description: 'Add your LinkedIn username.'
            },
            {
              name: 'linkedInLink',
              title: 'Link',
              type: 'url',
              placeholder: 'https://www.linkedin.com/in/username/',
              description: 'Add your LinkedIn profile url.'
            }
          ]
        }
      ]
    }
  ]
};
