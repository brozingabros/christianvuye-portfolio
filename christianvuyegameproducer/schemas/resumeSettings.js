import { FcFolder } from 'react-icons/fc';

export default {
  name: 'resumeSettings',
  title: 'Resume Settings',
  type: 'document',
  __experimental_actions: ['update', 'publish'],
  icon: FcFolder,
  fields: [
    {
      name: 'downloadCheck',
      description: 'Do you want add your resume to the resume page?',
      title: 'Activate Download Resume',
      type: 'boolean'
    },
    {
      name: 'downloadText',
      title: 'Download link text',
      description: 'The text shown for your download link',
      type: 'string'
    },
    { name: 'downloadFile', title: 'Add your resume', type: 'file' }
  ]
};
