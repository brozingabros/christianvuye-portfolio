import { FcViewDetails } from 'react-icons/fc';

export default {
  name: 'resume',
  title: 'Resume',
  type: 'document',
  __experimental_actions: ['update', 'publish'],
  icon: FcViewDetails,
  fields: [
    {
      name: 'title',
      description: 'Add a title',
      title: 'Title',
      type: 'string',
      validation: Rule => Rule.required().error('You need to add a title')
    },
    {
      name: 'body',
      title: 'Body',
      type: 'blockContent'
    },
    {
      name: 'column',
      description: 'Content block column',
      title: 'Column',
      type: 'string',
      options: {
        list: [
          { title: 'Left', value: 'left' },
          { title: 'Main', value: 'main' }
        ]
      },
      validation: Rule => Rule.required().error('You need to choose the column')
    },
    {
      name: 'order',
      title: 'Order',
      type: 'number',
      hidden: true
    }
  ],

  preview: {
    select: {
      title: 'title'
    },
    prepare(selection) {
      const { author } = selection;
      return { ...selection, subtitle: author && `by ${author}` };
    }
  }
};
