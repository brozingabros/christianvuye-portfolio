export default {
  widgets: [
    {
      name: 'netlify',
      options: {
        title: 'Netlify deploy status',
        sites: [
          {
            title: 'Sanity Studio',
            apiId: 'e0dacbcf-5e79-489d-9c6e-200e47cbf278',
            buildHookId: '5f77a0a9b71464013dcbc72f',
            name: 'studio-christianvuye'
          },
          {
            title: 'Website',
            apiId: 'a7cb9fbe-81f0-4418-b90c-09bd28863939',
            buildHookId: '5f77a0a9b71464013dcbc72f',
            name: 'christianvuye'
          }
        ]
      }
    }
  ]
};
