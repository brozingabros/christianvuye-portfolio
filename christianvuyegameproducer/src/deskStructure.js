import S from '@sanity/desk-tool/structure-builder';
import { FcBusinessContact, FcSettings, FcDocument, FcViewDetails } from 'react-icons/fc';

export default () =>
  S.list()
    .title('Content')
    .items([
      ...S.documentTypeListItems().filter(
        listItem => !['contactSettings', 'project', 'resume', 'about', 'resumeSettings'].includes(listItem.getId())
      ),
      S.listItem()
        .title('Pages')
        .icon(FcDocument)
        .child(
          S.list()
            .title('Pages')
            .items([
              S.listItem()
                .title('Projects')
                .schemaType('project')
                .child(S.documentTypeList('project').title('Projects')),
              S.listItem().title('Resume').icon(FcViewDetails).child(S.documentTypeList('resume').title('Resume')),
              S.listItem()
                .title('About')
                .icon(FcViewDetails)
                .child(S.editor().schemaType('about').documentId('about').title('About'))
            ])
        ),
      S.divider(),
      S.listItem()
        .title('Settings')
        .icon(FcSettings)
        .child(
          S.list()
            .title('Settings')
            .items([
              S.listItem()
                .title('Contact Settings')
                .icon(FcBusinessContact)
                .child(
                  S.editor().schemaType('contactSettings').documentId('contactSettings').title('Contact Settings')
                ),
              S.listItem()
                .title('Resume Settings')
                .icon(FcBusinessContact)
                .child(S.editor().schemaType('resumeSettings').documentId('resumeSettings').title('Resume Settings'))
            ])
        )
    ]);
